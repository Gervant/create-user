createNewUser = () => {
    const newUser = {
        userFirstName: prompt("Input your first name"),
        userLastName: prompt("Input your last name"),
        getLogin() {
            return `${this.firstName.slice(0, 1).toLowerCase()}${this.lastName.toLowerCase()}`
        },

        setFirstName(value) {
            this.userFirstName = value;
        },

        setLastName(value) {
            this.userLastName = value;
        },
    };

    Object.defineProperty(newUser, 'firstName', {
        get: function () {return this.userFirstName},

    });
    Object.defineProperty(newUser, 'lastName', {
        get: function () {return this.userLastName},

    });

    return newUser;
}

const object = createNewUser();
console.log(object)
console.log(object.getLogin())
